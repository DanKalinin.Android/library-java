//
// Created by Dan Kalinin on 9/19/20.
//

#ifndef LIBRARY_JAVA_JTINSTANT_H
#define LIBRARY_JAVA_JTINSTANT_H

#include "LJMain.h"

typedef struct {
    jclass class;
    jmethodID getEpochSecond;
    jmethodID ofEpochSecond;
    jmethodID parse;
} JTInstantType;

extern JTInstantType JTInstant;

void JTInstantLoad(JNIEnv *env);
void JTInstantUnload(JNIEnv *env);

#endif //LIBRARY_JAVA_JTINSTANT_H
