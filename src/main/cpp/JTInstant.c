//
// Created by Dan Kalinin on 9/19/20.
//

#include "JTInstant.h"

JTInstantType JTInstant = {0};

void JTInstantLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "java/time/Instant");
    JTInstant.class = (*env)->NewGlobalRef(env, class);
    JTInstant.getEpochSecond = (*env)->GetMethodID(env, class, "getEpochSecond", "()J");
    JTInstant.ofEpochSecond = (*env)->GetStaticMethodID(env, class, "ofEpochSecond", "(J)Ljava/time/Instant;");
    JTInstant.parse = (*env)->GetStaticMethodID(env, class, "parse", "(Ljava/lang/CharSequence;)Ljava/time/Instant;");
}

void JTInstantUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, JTInstant.class);
}
