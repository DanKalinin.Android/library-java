//
// Created by Dan Kalinin on 9/11/20.
//

#ifndef LIBRARY_JAVA_JAVAJNI_H
#define LIBRARY_JAVA_JAVAJNI_H

#include "LJMain.h"
#include "JTInstant.h"

void LJJNIEnvCallVoidMethod(JNIEnv *env, jobject this, jmethodID methodID, ...);
void LJJNIEnvCallVoidMethodV(JNIEnv *env, jobject this, jmethodID methodID, va_list args);

char *LJJNIEnvGetStringUTFChars(JNIEnv *env, jstring this, jboolean *isCopy);
void LJJNIEnvReleaseStringUTFChars(JNIEnv *env, jstring this, char *utf);

jsize LJJNIEnvGetArrayLength(JNIEnv *env, jarray this);

#endif //LIBRARY_JAVA_JAVAJNI_H
