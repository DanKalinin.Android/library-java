//
// Created by Dan Kalinin on 9/11/20.
//

#include "LJJNI.h"

void LJJNIEnvCallVoidMethod(JNIEnv *env, jobject this, jmethodID methodID, ...) {
    va_list args;
    va_start(args, methodID);
    LJJNIEnvCallVoidMethodV(env, this, methodID, args);
    va_end(args);
}

void LJJNIEnvCallVoidMethodV(JNIEnv *env, jobject this, jmethodID methodID, va_list args) {
    if (this == NULL) return;

    (*env)->CallVoidMethodV(env, this, methodID, args);
}

char *LJJNIEnvGetStringUTFChars(JNIEnv *env, jstring this, jboolean *isCopy) {
    if (this == NULL) return NULL;

    char *ret = (char *)(*env)->GetStringUTFChars(env, this, isCopy);
    return ret;
}

void LJJNIEnvReleaseStringUTFChars(JNIEnv *env, jstring this, char *utf) {
    if (this == NULL) return;

    (*env)->ReleaseStringUTFChars(env, this, utf);
}

jsize LJJNIEnvGetArrayLength(JNIEnv *env, jarray this) {
    if (this == NULL) return 0;

    jsize ret = (*env)->GetArrayLength(env, this);
    return ret;
}

jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (void **)&env, JNI_VERSION_1_2);

    JTInstantLoad(env);

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, void *reserved) {
    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (void **)&env, JNI_VERSION_1_2);

    JTInstantUnload(env);
}
