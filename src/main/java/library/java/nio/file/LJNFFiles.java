package library.java.nio.file;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import library.java.lang.LJLObject;

public class LJNFFiles extends LJLObject {
    public static int copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        int ret = 0;
        int n = 0;
        byte[] b = new byte[1024];

        while ((n = inputStream.read(b)) > 0) {
            outputStream.write(b, 0, n);
            ret += n;
        }

        return ret;
    }
}
