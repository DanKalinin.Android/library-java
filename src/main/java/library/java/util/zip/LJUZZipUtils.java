package library.java.util.zip;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import library.java.lang.LJLObject;
import library.java.nio.file.LJNFFiles;

public class LJUZZipUtils extends LJLObject {
    public static int unzip(ZipInputStream inputStream, File directory) throws IOException {
        int ret = 0;
        ZipEntry entry = null;

        while ((entry = inputStream.getNextEntry()) != null) {
            File file = new File(directory, entry.getName());

            if (entry.isDirectory()) {
                file.mkdirs();
            } else {
                file.getParentFile().mkdirs();

                try (FileOutputStream outputStream = new FileOutputStream(file)) {
                    ret += LJNFFiles.copy(inputStream, outputStream);
                }
            }
        }

        return ret;
    }
}
