package library.java.lang;

public class LJLException extends Exception {
    public LJLException(String message) {
        super(message);
    }
}
