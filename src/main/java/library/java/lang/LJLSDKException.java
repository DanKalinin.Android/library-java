package library.java.lang;

public class LJLSDKException extends LJLException {
    public String domain;
    public int code;

    public LJLSDKException(String domain, int code, String message) {
        super(message);

        this.domain = domain;
        this.code = code;
    }
}
